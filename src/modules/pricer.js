import { getPricingData } from '../__mocks__/pricing'
import { discountCodes } from '../__mocks__/discountCodes'
import { discountIt } from '../utils/discountIt'

export const PRICING_REQUESTED = 'PRICING_REQUESTED'
export const PRICING_FETCHED = 'PRICING_FETCHED'

export const DISCOUNT_REQUESTED = 'DISCOUNT_REQUESTED'
export const DISCOUNT_APPLIED = 'DISCOUNT_APPLIED'
export const DISCOUNT_FAILED = 'DISCOUNT_FAILED'

export const pricingRequested = () => ({ type: PRICING_REQUESTED })
export const pricingFetched = data => ({ type: PRICING_FETCHED, data })

export const discountFailed = () => ({ type: DISCOUNT_FAILED})
export const discountRequested = () => ({ type: DISCOUNT_REQUESTED })
export const discountApplied = () => ({ type: DISCOUNT_APPLIED })

export const validateDiscount = (code) => {
  return dispatch => {
    dispatch(discountRequested())
    if(discountCodes[code]) {
      
      getPricingData().then(data => {
        console.log(discountCodes[code])
        data.discounted = {
          ...data.pricing,
          subtotal:discountIt(discountCodes[code],data.pricing.subtotal),
          tax:discountIt(discountCodes[code],data.pricing.tax)
        }
        data.discounted.total = data.discounted.subtotal +
        data.discounted.tax -
        data.discounted.savings
        
        dispatch(discountApplied())
        dispatch(pricingFetched(data))
      })
    } else {
      dispatch(discountFailed())
    }

  }

}
export const fetchPricing = () => {
  return dispatch => {
    dispatch(pricingRequested())

    getPricingData().then(data => {
      dispatch(pricingFetched(data))
    })
  }
}

const initialState = {
  inProgress: false,
  data: null,
  isDiscounted: false,
  isDiscounting: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case PRICING_REQUESTED:
      return {
        ...state,
        inProgress: true
      }

    case PRICING_FETCHED:
      return {
        ...state,
        data: action.data,
        inProgress: !state.inProgress
      }

    case DISCOUNT_REQUESTED:
      return {
        ...state,
        isDiscounting: true,
        isDiscounted: false
      }
      case DISCOUNT_FAILED:
        return {
          ...state,
          isDiscounting: false,
          isDiscounted: false
        }
      
      case DISCOUNT_APPLIED:
        return {
          ...state,
          isDiscounting: false,
          isDiscounted: true
        }


    default:
      return state
  }
}

// export const incrementAsync = () => {
//   return dispatch => {
//     dispatch({
//       type: PRICING_REQUESTED
//     })
//
//     return setTimeout(() => {
//       dispatch({
//         type: PRICING
//       })
//     }, 3000)
//   }
// }
//
// export const decrement = () => {
//   return dispatch => {
//     dispatch({
//       type: DISCOUNT_REQUESTED
//     })
//
//     dispatch({
//       type: DISCOUNT
//     })
//   }
// }
//
// export const decrementAsync = () => {
//   return dispatch => {
//     dispatch({
//       type: DISCOUNT_REQUESTED
//     })
//
//     return setTimeout(() => {
//       dispatch({
//         type: DISCOUNT
//       })
//     }, 3000)
//   }
// }
