import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './store'
import App from './containers/app'
import 'sanitize.css/sanitize.css'
import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
 html {
  font-size: 100%;
}

body {
  margin: 0;
  padding: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif;
  font-size: 1rem;
  line-height: 1.5;
}

.strike {text-decoration: line-through;display:block;}

.center {  
  display: flex;
  align-items: center;
  justify-content: center;
}


.bold {font-weight:bold;}
.grey {color:#888;}

.savings {
  color:red;
  &:before{ content: "-";}
}
.summary-product {
  max-height:50px;
  margin-right: 1rem;
}

.dollar{
  &:before {
      content: '$';
  }
  
  &.savings:before {
      content: '-$';
  }
}
.btn {
  background-color: #fff;
  border: none;
  color: #222;
  border:solid 3px #222;
  font-weight:bold;
  border-radius:40px;
  padding: 0.25rem 0.5rem;
  text-decoration: none;
  margin: 4px 2px;
  cursor: pointer;
  &.btn-apply {
    margin: 1.3rem 0.5rem;
  }
}
.underline {text-decoration: underline;}

[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
  
}


[data-tooltip]:before,
[data-tooltip]:after {
  
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}


[data-tooltip]:before {
  text-decoration: none;
  position: absolute;
  top: 125%;
  left: 5rem;
  text-align: left;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 1rem;
  min-width: 200px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #fff;
  
  border: solid 1px #ccc;
  color: #888;
  content: attr(data-tooltip);
  text-align: left;
  font-size: 14px;
  line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
  position: absolute;
  left: 5rem;
  top: 100%;
  margin-left: -5px;
  width: 0;
  height: 0;
  content: " ";
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid white;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
}
`

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <App />
        <GlobalStyle />
      </div>
    </ConnectedRouter>
  </Provider>,
  target
)
