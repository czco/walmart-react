Number.prototype.toFixedNumber = function(x, base){
  var pow = Math.pow(base||10,x);
  return Math.round(this*pow) / pow;
}


export const discountIt = (percent, total) => {
  return parseFloat(((100 - percent) / 100) * total).toFixedNumber(2);
}