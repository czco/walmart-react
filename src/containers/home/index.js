import React, { Component } from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchPricing, validateDiscount } from '../../modules/pricer'
import '../../assets/devices.css'

import MobileContainer from '../../components/MobileContainer'
import Collapsible from '../../components/Collapsible'
import { Divider, Flex, Summary } from '../../components/styled/common'
import { Discounter } from '../../components/Discounter'

class Home extends Component {
  componentDidMount() {
    this.props.fetchPricing()
  }

  render() {
    const { pricing, itemDetails, validateDiscount, discounted } = this.props
    console.log('pricing', pricing)
    console.log('itemDetails', itemDetails)
    return (
      <div className={'center'}>
        <MobileContainer>
          <Summary>
            {pricing && (
              <>
                <Flex>
                  <span>Subtotal</span>{' '}
                  <span className="bold dollar">
                    {discounted ? discounted.subtotal : pricing.subtotal}
                  </span>
                </Flex>
                <Flex>
                  <span
                    data-tooltip="Picking up order in the store helps cut costs and we pass on the savings to you"
                    className="underline">
                    Pickup Savings
                  </span>{' '}
                  <span className="bold savings dollar">{pricing.savings}</span>
                </Flex>
                <Flex>
                  <span>
                    Est. taxes & fees
                    <br />
                    (Based on {pricing.zip})
                  </span>
                  <span className="bold dollar">
                    {discounted ? discounted.tax : pricing.tax}
                  </span>
                </Flex>
                <Flex className={'large'}>
                  <span className="bold">Est. total</span>{' '}
                  <span className="bold dollar">
                    {discounted
                      ? discounted.total
                      : pricing.total}
                  </span>
                </Flex>
              </>
            )}

            <Divider />
            {itemDetails && (
              <Collapsible
                titleClosed={'See Item details'}
                titleOpen={'Hide Item details'}>
                <Flex>
                  <img alt="" className="summary-product" src={itemDetails.imgUrl} />{' '}
                  <span>
                    {itemDetails.item_name}
                    <Flex>
                      <div>
                        <span
                          className={
                            'bold dollar' + (discounted ? ' grey strike' : '')
                          }>
                          {pricing.subtotal}
                        </span>
                        {discounted && (
                          <span className={'bold dollar'}>
                            {discounted.subtotal}
                          </span>
                        )}
                      </div>
                      <span>Qty:1</span>
                    </Flex>
                  </span>
                </Flex>
              </Collapsible>
            )}
            <Divider />
            {pricing && (
              <Collapsible
                titleClosed={'Apply promo code'}
                titleOpen={'Hide promo code'}>
                <Discounter onSubmit={validateDiscount} />
              </Collapsible>
            )}
          </Summary>
        </MobileContainer>
      </div>
    )
  }
}

const mapStateToProps = ({ pricer }) => ({
  discounted: pricer.data && pricer.data.discounted,
  pricing: pricer.data && pricer.data.pricing,
  itemDetails: pricer.data && pricer.data.itemDetails,
  inProgress: pricer.inProgress
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchPricing,
      validateDiscount,
      changePage: () => push('/about-us')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
