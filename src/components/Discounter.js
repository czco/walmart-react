import React, { useState } from 'react'
import { Flex,Input } from './styled/common'
import * as PropTypes from 'prop-types'

export const Discounter = props => {
  const [discountCode, setCode] = useState()

  const handleChange = e => {
    setCode(e.target.value)
  }

  const handleSubmit = event => {
    if(discountCode) {
      props.onSubmit(discountCode)
    }
    event.preventDefault()
  }


  return <form onSubmit={handleSubmit}>
    <Flex>
      <div>
        <label htmlFor="input-promo">Promo Code</label>
        <Input id="input-promo"
               onChange={handleChange} required type="text" />
      </div>
      <div>
        <input type="submit" className={'btn btn-apply'} value="Apply"/>
      </div>
    </Flex>
  </form>
}

Discounter.propTypes = { onSubmit: PropTypes.func }