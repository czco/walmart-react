
import styled from 'styled-components'

export const Divider = styled.hr``
export const Summary = styled.div`
  margin-left: auto;
  margin-right: 5%;
  padding: 1rem 1.5rem;
  width: 80%;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  border: solid 1px #ccc;
`
export const Input = styled.input`
  padding: 0.5rem 0.1rem;
`

export const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 0.8rem;
  &.large {
    font-size: 1.4rem;
  }
`
