import React, { Component } from 'react'
import styled from 'styled-components'
const Title = styled.div`
  text-decoration: underline;
  cursor: pointer;
  margin:1rem 0;
  &:after {
    background-image: url('https://img.icons8.com/android/24/000000/plus.png');
    background-size: 15px 15px;
    display: inline-block;
    width: 15px; 
    height: 15px;
    content:"";
    margin-left: 5px;
  }
  &.open:after {
    background-image: url('https://img.icons8.com/android/24/000000/minus.png');
  }
`

class Collapsible extends Component {
  state = {
    isOpen: false
  }
  handleToggle = () => {

    this.setState(prevState => {
      
      return ({
        isOpen: !prevState.isOpen
      })
    });
  }
  render() {
    const { children, titleClosed,titleOpen } = this.props
    const { isOpen } = this.state
    return (
      <div>
          <Title onClick={this.handleToggle} className={!isOpen ? 'closed' : 'open'}>{!isOpen ? titleClosed : titleOpen}</Title>
        {isOpen && children}
      </div>
    )
  }
}

export default Collapsible
